# Bosnian translation for bosnianuniversetranslation
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the bosnianuniversetranslation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: bosnianuniversetranslation\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2015-02-15 13:02+0000\n"
"Last-Translator: Samir Ribić <Unknown>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-16 06:46+0000\n"
"X-Generator: Launchpad (build 17341)\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: contents/ui/ConnectionItem.qml:60
#, kde-format
msgid "Connect"
msgstr "Spoji"

#: contents/ui/ConnectionItem.qml:60
#, kde-format
msgid "Disconnect"
msgstr "Prekini vezu"

#: contents/ui/ConnectionItem.qml:85
#, kde-format
msgid "Show Network's QR Code"
msgstr ""

#: contents/ui/ConnectionItem.qml:90
#, kde-format
msgid "Configure…"
msgstr ""

#: contents/ui/ConnectionItem.qml:121
#, kde-format
msgid "Speed"
msgstr ""

#: contents/ui/ConnectionItem.qml:126
#, kde-format
msgid "Details"
msgstr ""

#: contents/ui/ConnectionItem.qml:169
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Current download speed is %1 kibibytes per second; current upload speed is "
"%2 kibibytes per second"
msgstr ""

#: contents/ui/ConnectionItem.qml:286
#, kde-format
msgid "Connected, ⬇ %1/s, ⬆ %2/s"
msgstr "Povezano, ⬇ %1/s, ⬆ %2/s"

#: contents/ui/ConnectionItem.qml:290
#, kde-format
msgid "Connected"
msgstr "Povezano"

#: contents/ui/DetailsText.qml:45
#, kde-format
msgid "Copy"
msgstr ""

#: contents/ui/main.qml:24
#, kde-format
msgid "Networks"
msgstr "Mreže"

#: contents/ui/main.qml:33
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn off Airplane Mode"
msgstr ""

#: contents/ui/main.qml:35
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn on Airplane Mode"
msgstr ""

#: contents/ui/main.qml:79 contents/ui/Toolbar.qml:69
#, kde-format
msgid "Enable Wi-Fi"
msgstr ""

#: contents/ui/main.qml:87
#, kde-format
msgid "Enable Mobile Network"
msgstr ""

#: contents/ui/main.qml:95
#, kde-format
msgid "Enable Airplane Mode"
msgstr ""

#: contents/ui/main.qml:101
#, kde-format
msgid "Open Network Login Page…"
msgstr ""

#: contents/ui/main.qml:106
#, kde-format
msgid "&Configure Network Connections…"
msgstr ""

#: contents/ui/main.qml:143
#, kde-format
msgctxt "@title:window"
msgid "QR Code for %1"
msgstr ""

#: contents/ui/PasswordField.qml:16
#, fuzzy, kde-format
#| msgid "Password..."
msgid "Password…"
msgstr "Lozinka..."

#: contents/ui/PopupDialog.qml:102
#, kde-format
msgid "Airplane mode is enabled"
msgstr ""

#: contents/ui/PopupDialog.qml:106
#, kde-format
msgid "Wireless and mobile networks are deactivated"
msgstr ""

#: contents/ui/PopupDialog.qml:108
#, kde-format
msgid "Wireless is deactivated"
msgstr ""

#: contents/ui/PopupDialog.qml:111
#, kde-format
msgid "Mobile network is deactivated"
msgstr ""

#: contents/ui/PopupDialog.qml:114
#, kde-format
msgid "No matches"
msgstr ""

#: contents/ui/PopupDialog.qml:116
#, fuzzy, kde-format
#| msgid "Available connections"
msgid "No available connections"
msgstr "Dostupne konekcije"

#: contents/ui/Toolbar.qml:118
#, kde-format
msgid "Enable mobile network"
msgstr ""

#: contents/ui/Toolbar.qml:143
#, kde-kuit-format
msgctxt "@info"
msgid "Disable airplane mode<nl/><nl/>This will enable Wi-Fi and Bluetooth"
msgstr ""

#: contents/ui/Toolbar.qml:144
#, kde-kuit-format
msgctxt "@info"
msgid "Enable airplane mode<nl/><nl/>This will disable Wi-Fi and Bluetooth"
msgstr ""

#: contents/ui/Toolbar.qml:154
#, kde-format
msgid "Hotspot"
msgstr ""

#: contents/ui/Toolbar.qml:178 contents/ui/Toolbar.qml:189
#, kde-format
msgid "Disable Hotspot"
msgstr ""

#: contents/ui/Toolbar.qml:183 contents/ui/Toolbar.qml:189
#, kde-format
msgid "Create Hotspot"
msgstr ""

#: contents/ui/Toolbar.qml:221
#, kde-format
msgid "Configure network connections…"
msgstr ""

#: contents/ui/TrafficMonitor.qml:36 contents/ui/TrafficMonitor.qml:106
#, kde-format
msgid "/s"
msgstr ""

#: contents/ui/TrafficMonitor.qml:88
#, kde-format
msgid "Upload"
msgstr ""

#: contents/ui/TrafficMonitor.qml:88
#, kde-format
msgid "Download"
msgstr ""

#, fuzzy
#~| msgid "Connect"
#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Connect to %1"
#~ msgstr "Spoji"

#, fuzzy
#~| msgid "Disconnect"
#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Disconnect from %1"
#~ msgstr "Prekini vezu"

#, fuzzy
#~| msgid "Available connections"
#~ msgctxt "button tooltip"
#~ msgid "Search the connections"
#~ msgstr "Dostupne konekcije"

#~ msgid "Available connections"
#~ msgstr "Dostupne konekcije"

#, fuzzy
#~| msgid "Available connections"
#~ msgid "Show virtual connections"
#~ msgstr "Dostupne konekcije"

#~ msgid "Show password"
#~ msgstr "Prikaži lozinku"
